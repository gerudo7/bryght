// Imports
const Discord = require("discord.js")
const client = new Discord.Client()
const { Client, MessageEmbed } = require('discord.js');
const { DiscordInteractions } = require("slash-commands");
require('dotenv').config() // Comment this out if you want to manage environment manually.

// Setup for slash commands.
const interaction = new DiscordInteractions({
    applicationId: "673302906740408320",
    authToken: process.env.TOKEN,
    publicKey: "417b6f2635d9dd0d9af59ff436b418ceb8d92357ef8e1a102f36e5a5feb89a56",
});
 
// Set variables
const prefix = "&"
const helpContent = new MessageEmbed()
helpContent.setTitle('Commands') // Set the title of the field
helpContent.setColor(0x01773e) // Set the color of the embed
helpContent.setDescription('`help` - This menu \n `ping` - pong (test program)"'); // Set the main content of the embed

// honestly not sure what this does but it's important
client.on("ready", () => {
  console.log(`Logged in as ${client.user.tag}!`)
})

// demo function
client.on("message", msg => {
  if (msg.content === "ping") {
    msg.reply("pong");
  }
})

// real functions
client.on("message", msg => {
    if (msg.content === prefix+"help") {
        const helpContent = new MessageEmbed()
        helpContent.setTitle('Commands') // Set the title of the field
        helpContent.setColor(0x01773e) // Set the color of the embed
        helpContent.setDescription('`help` - This menu \n `ping` - pong (test program)"'); // Set the main content of the embed
        msg.channel.send(helpContent);
    }
  })

// slash commands library ----------------------------------------------------------------------

client.on('ready', () => {
    client.api.applications(client.user.id).commands.post({
        data: {
            name: "ping",
            description: "pong"
            // possible options here e.g. options: [{...}]
        }
    });


    client.ws.on('INTERACTION_CREATE', async interaction => {
        const command = interaction.data.name.toLowerCase();
        const args = interaction.data.options;
        if (command === "ping"){ 
            client.api.interactions(interaction.id, interaction.token).callback.post({
                data: {
                    type: 4,
                    data: {
                        content: "pong"
                    }
                }
            })
        }
    });
});

client.on('ready', () => {
    client.api.applications(client.user.id).commands.post({
        data: {
            name: "help",
            description: "Show help menu."
            // possible options here e.g. options: [{...}]
        }
    });


    client.ws.on('INTERACTION_CREATE', async interaction => {
        const command = interaction.data.name.toLowerCase();
        const args = interaction.data.options;

        if (command === 'help'){ 
            // here you could do anything. in this sample
            // i reply with an api interaction
            // here you could do anything. in this sample
            // i reply with an api interaction
            const helpContent = new MessageEmbed()
            helpContent.setTitle('Commands') // Set the title of the field
            helpContent.setColor(0x01773e) // Set the color of the embed
            helpContent.setDescription('`help` - This menu \n `ping` - pong (test program)"'); // Set the main content of the embed
            client.api.interactions(interaction.id, interaction.token).callback.post({
                data: {
                    type: 4,
                    data: {
                        embeds: [ helpContent ]
                    }        
                }
            })
        }
    });
});

// --------------------------------------------------------------------------------------------------------------

// run!
client.login(process.env.TOKEN)